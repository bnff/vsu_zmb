# подключаем нашу библиотку
from VSU.JMB.LinearHashtable import LinearHashtable
# подключаем рандом
import random


def main():
    random_arr = False
    # размер массива
    size_line = 32
    if random_arr:
        # генерация элементов
        insert_values = (random.randint(0, int(r_t + 199 / 5)) for r_t in range(0, size_line))
    else:
        insert_values = [16, 21, 21, 40, 9, 23, 7, 13, 17, 24, 46, 30, 25, 18, 7, 45, 13, 26, 53, 54, 46, 29, 37, 37,
                         63, 44, 18, 49, 50, 28, 68, 17, 0]
    # инициализация класса
    hash_line = LinearHashtable(size_line, False)
    for value in insert_values:
        # печатаем элементы
        print(value, end=' ')
        # добавляем элементы
        hash_line.insert(value)
    # выводи что у нас получилось
    print()
    print(hash_line)


if __name__ == '__main__':
    main()
